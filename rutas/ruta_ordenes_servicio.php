<?php   defined('ACCESSO') or die('No direct script access.');



/**===================================
 *        Rutas para:
 *        Ordenes de Servicio
 ====================================*/

/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/os_servicio/json/get/clientes', function () {
  isLogin(); // comprueba si esta logueado
  $clientes = App::Db()->select('clientes',array('c_nombre','c_apellidos','cliente_id','c_tlf'));
  $data = array(); // iniciamos el array
  foreach ($clientes as $cliente) {// obtenemos todos los objetos
    $data[] = array(
      'cliente_id' => $cliente['cliente_id'],
      'nombre' => $cliente['c_nombre'].' '.$cliente['c_apellidos'],
      'tlf' => $cliente['c_tlf']
    );
  }
  return App::Json($data); // retorna en formato json para el javascript
});


/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/os_servicio/json/get/productos', function () {
  isLogin(); // comprueba si esta logueado
  $productos = App::Db()->select('productos',array('nombre','producto_id','precio_venta'));
  $data = array(); // iniciamos el array
  foreach ($productos as $producto) {// obtenemos todos los objetos
    $data[] = array(
      'producto_id' => $producto['producto_id'],
      'nombre' => $producto['nombre'].' - '.$producto['precio_venta'].'€',
      'precio_venta' => $producto['precio_venta']
    );
  }
  return App::Json($data); // retorna en formato json para el javascript
});


/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/os_servicio/json/get/servicios', function () {
  isLogin(); // comprueba si esta logueado
  $servicios = App::Db()->select('servicios',array('nombre','servicio_id','precio'));
  $data = array(); // iniciamos el array
  foreach ($servicios as $servicio) {// obtenemos todos los objetos
    $data[] = array(
      'servicio_id' => $servicio['servicio_id'],
      'nombre' => $servicio['nombre'].' - '.$servicio['precio'].'€',
      'precio' => $servicio['precio']
    );
  }
  return App::Json($data); // retorna en formato json para el javascript
});

/**
*  @todo: para la busqueda de ordenes retorna en formato json
*/
$App->Route('/gestion/os_servicio/json/get/ordenes', function () {
  isLogin(); // comprueba si esta logueado
  $os_servicio = App::Db()->query("SELECT c_nombre,c_apellidos,os_id,c_tlf FROM ordenes_servicio INNER JOIN clientes ON ordenes_servicio.cliente_id = clientes.cliente_id,usuarios")->fetchAll();
  $data = array(); // iniciamos el array
  foreach ($os_servicio as $os) {// obtenemos todos los objetos
    $data[] = array(
      'id' => $os['os_id'],
      'tlf'=> $os['c_tlf'],
      'nombre' => $os['c_nombre'].' '.$os['c_apellidos'],
    );
  }
  return App::Json($data); // retorna en formato json para el javascript
});



/**
*  @todo: nueva orden
*/
$App->Route('/gestion/os_servicio/nuevo', function () {
  isLogin(); // comprueba si esta logueado

  // boton  nuevo cliente
  if(App::Post('saveNewClient')){
    // guardamos datos si todo es correcto
    $data = array(
      'c_nombre' => ucfirst(App::Post('name')),
      'c_apellidos' => App::Post('last_name'),
      'c_dni' => App::Post('dni'),
      'c_email' => App::Post('email'),
      'c_direccion' => App::Post('address'),
      'c_ciudad' => App::Post('locality'),
      'c_cp' => App::Post('cp'),
      'c_observaciones' => App::Post('observations'),
      'c_tlf' => App::Post('tlf'),
      'c_fecha' => Date('d/m/Y'),
    );
    // si todo va bien guardamos y refrescamos pagina
    setDb('clientes',$data,'/gestion/os_servicio/nuevo');
  }
  // boton guardar os_servicio
  if(App::Post('saveServiceOrder')){
    // guardamos datos si todo es correcto
    $data = array(
      'fecha' => date('d/m/Y'),
      'fecha_inicial' => (App::Post('fecha_inicial')) ? App::Post('fecha_final') : Date('d/m/Y'),
      'fecha_final' => App::Post('fecha_final'),
      'fecha_entrega' => App::Post('fecha_entrega'),
      'sintomas' => App::Post('sintomas'),
      'reparacion' => App::Post('reparacion'),
      'diagnostico' => App::Post('diagnostico'),
      'garantia' => App::Post('garantia'),
      'estado' => App::Post('estado'),
      'total' => App::Post('total'),
      'cliente_id' => App::Post('cliente_id'),
      'usuario_id' => App::Post('usuario_id'),
      'cds' => App::Post('cds'),
      'cargador' => App::Post('cargador'),
      'maletin' => App::Post('maletin'),
      'otros' => App::Post('otros'),
      'descripcion_equipo' => App::Post('descripcion_equipo')
    );
    // si todo va  guardamos y redirecionamos a la seccion editar
    $db = App::Db()->insert('ordenes_servicio',$data);
    if($db){
      App::SetMsg(App::$lang['success'],App::$lang['the_file_has_been_added']);
      App::Redirect(App::Url().'/gestion/os_servicio/editar/'.$db.'#orders');
    }
  }

  $file = 'nueva/nueva_os_servicio.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: editar orden
*/
$App->Route('/gestion/os_servicio/editar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado

  // obtenomos todos las ordenes de servicio con los usuarios y los clientes
  $ordenes_servicio = App::Db()->query("
    SELECT * FROM ordenes_servicio
    JOIN clientes ON  ordenes_servicio.cliente_id = clientes.cliente_id
    JOIN usuarios ON  ordenes_servicio.usuario_id = usuarios.usuario_id
    WHERE os_id  IS  $id
  ")->fetchAll();
  $item = $ordenes_servicio[0];

  // obtenemos todos los productos de la orden de servicio
  $os_producto  = App::Db()->query("
    SELECT * FROM os_producto
    JOIN productos ON  os_producto.producto_id = productos.producto_id
    WHERE os_id  IS $id
  ")->fetchAll();
  // obtenemos todos los servicios de la orden de servicio
  $os_servicio  = App::Db()->query("
    SELECT * FROM os_servicio
    JOIN servicios ON  os_servicio.servicio_id = servicios.servicio_id
    WHERE os_id  IS $id
  ")->fetchAll();


  // añadimos producto
  if(App::Post('addOsProduct')){
    // obtenmemos todos los precio_venta de los productos con el id del producto_id
    $producto = App::Db()->get('productos',array('precio_venta'),array('producto_id' => App::Post('producto_id')));
    $producto['precio_venta'] = str_replace(',','.',$producto['precio_venta']);
    // array para guardad
    $data = array(
      'cantidad' => App::Post('cantidad'),
      'os_id' => $id,
      'producto_id' => App::Post('producto_id'),
      'subtotal' => number_format(App::Post('cantidad') * $producto['precio_venta'],2)
    );
    // si no esta vacio el producto_id y la cantidad es mayor que 0 guardamos y refrescamos pagina
    if(App::Post('producto_id') and App::Post('cantidad') > 0) {
      // quitamos las unidades del producto
      App::Db()->update('productos',array('stock[-]' => $data['cantidad']),array('producto_id' => $data['producto_id']));
      setDb('os_producto',$data,'/gestion/os_servicio/editar/'.$id.'#products');
    }
  }

  // añadimos servicio
  if(App::Post('addOsService')){
    // obtenmemos todos los precio_venta de los productos con el id del producto_id
    $servicio = App::Db()->get('servicios',array('precio'),array('servicio_id' => App::Post('servicio_id')));
    // cambiamos . por , si lo hay
    $servicio['precio'] = str_replace(',','.',$servicio['precio']);
    // array para guardad
    $data = array(
      'os_id' => $id,
      'servicio_id' => App::Post('servicio_id'),
      'subtotal' => $servicio['precio']
    );
    // si no esta vacio el producto_id  guardamos y refrescamos pagina
    if(App::Post('servicio_id')) setDb('os_servicio',$data,'/gestion/os_servicio/editar/'.$id.'#services');
  }

  // sumamos el total de los productos y servicios
  $totalProductos = App::Db()->query("SELECT SUM(subtotal) AS total FROM os_producto WHERE os_id IS $id")->fetchAll();
  $totalServicios = App::Db()->query("SELECT SUM(subtotal) AS total FROM os_servicio WHERE os_id IS $id")->fetchAll();
  // enseñamos el total de los productos y servicios
  $productosTotal = number_format((float) $totalProductos[0]['total'],2);
  $serviciosTotal = number_format((float) $totalServicios[0]['total'],2);


  // submit botton os_servicio
  if(App::Post('editOsService')){

    // guardamos datos si todo es correcto
    $data = array(
      'fecha' => $item['fecha'],
      'fecha_inicial' => (App::Post('fecha_inicial')) ? App::Post('fecha_inicial') : $item['fecha_inicial'],
      'fecha_final' => (App::Post('fecha_final')) ? App::Post('fecha_final') : $item['fecha_final'],
      'fecha_entrega' => (App::Post('fecha_entrega')) ? App::Post('fecha_entrega') : $item['fecha_entrega'],
      'sintomas' => (App::Post('sintomas')) ? App::Post('sintomas') : $item['sintomas'],
      'reparacion' => App::Post('reparacion'),
      'diagnostico' => App::Post('diagnostico'),
      'garantia' => App::Post('garantia'),
      'estado' => (App::Post('estado')) ? App::Post('estado') : $item['estado'],
      'total' => (App::Post('total')) ? App::Post('total') : $item['total'],
      'cliente_id' => (App::Post('cliente_id')) ? App::Post('cliente_id') : $item['cliente_id'],
      'usuario_id' => (App::Post('usuario_id')) ? App::Post('usuario_id') : $item['usuario_id'],
      'cds' => App::Post('cds'),
      'cargador' => App::Post('cargador'),
      'maletin' => App::Post('maletin'),
      'otros' => App::Post('otros'),
      'descripcion_equipo' => (App::Post('descripcion_equipo')) ? App::Post('descripcion_equipo') : $item['descripcion_equipo']
    );
    // actualizamos os_servicio
    putDb('ordenes_servicio',$data,'os_id',$id,'/gestion/os_servicio/editar/'.$id);
  }


  $file = 'editar/editar_os_servicio.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});

/**
*  @todo: preview orden
*/
$App->Route('/gestion/os_servicio/preview/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  $empresa = App::Db()->get('empresa','*');
  $logo = App::Url().'/imagenes/empresa/'.$empresa['logo'];
  $details = str_replace("\n",'<br>',$empresa['descripccion']);
  $terms = $empresa['terminos'];

  // obtenomos todos las ordenes de servicio con los usuarios y los clientes
  $ordenes_servicio = App::Db()->query("
    SELECT * FROM ordenes_servicio
    JOIN clientes ON  ordenes_servicio.cliente_id = clientes.cliente_id
    JOIN usuarios ON  ordenes_servicio.usuario_id = usuarios.usuario_id
    WHERE os_id  IS  $id
  ")->fetchAll();
  $item = $ordenes_servicio[0];
  // obtenemos todos los productos de la orden de servicio
  $os_producto  = App::Db()->query("
    SELECT * FROM os_producto
    JOIN productos ON  os_producto.producto_id = productos.producto_id
    WHERE os_id  IS $id
  ")->fetchAll();
  // obtenemos todos los servicios de la orden de servicio
  $os_servicio  = App::Db()->query("
    SELECT * FROM os_servicio
    JOIN servicios ON  os_servicio.servicio_id = servicios.servicio_id
    WHERE os_id  IS $id
  ")->fetchAll();

  // sumamos el total de los productos y servicios
  $totalProductos = App::Db()->query("SELECT SUM(subtotal) AS total FROM os_producto WHERE os_id IS $id")->fetchAll();
  $totalServicios = App::Db()->query("SELECT SUM(subtotal) AS total FROM os_servicio WHERE os_id IS $id")->fetchAll();
  // enseñamos el total de los productos y servicios
  $productosTotal = number_format((float) $totalProductos[0]['total'],2);
  $serviciosTotal = number_format((float) $totalServicios[0]['total'],2);

  // incluimos el archivo html
  include PLANTILLA.'/preview_os_servicio.html';
});

/**
*  @todo: borrar os_producto
*/
$App->Route('/gestion/os_servicio/borrar/os_producto/(:num)/id/(:num)/cantidad/(:num)/producto_id/(:num)', function ($id_producto,$id,$cantidad,$producto_id) {
  isLogin(); // comprueba si esta logueado
  // devolvemos la cantidad al stock
  $db = App::Db()->update('productos',array('stock[+]' => $cantidad),array('producto_id' => $producto_id));
  if($db){
    // borramos el producto de las ordenes de servicio
    delDb('os_producto','os_producto_id',$id,'/gestion/os_servicio/editar/'.$id_producto.'#products');
  }
});
/**
*  @todo: borrar os_service
*/
$App->Route('/gestion/os_servicio/borrar/os_servicio/(:num)/id/(:num)', function ($id_servicio,$id) {
  isLogin(); // comprueba si esta logueado
  // borramos el servicio de las ordenes de servicio
  delDb('os_servicio','os_servicio_id',$id,'/gestion/os_servicio/editar/'.$id_servicio.'#services');
});

/**
*  @todo: borrar orden y os_producto ,os_service
*/
$App->Route('/gestion/os_servicio/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos la orden de servicio y todos los productos y servicios de esa orden
  App::Db()->query("DELETE FROM ordenes_servicio WHERE os_id IS $id")->fetchAll();
  App::Db()->query("DELETE FROM os_producto WHERE os_id IS $id")->fetchAll();
  App::Db()->query("DELETE FROM os_servicio WHERE os_id IS $id")->fetchAll();
  App::Redirect(App::Url().'/gestion/os_servicio');
});




/**
*  @todo: ordenes de servicio
*/
$App->Route(array('/gestion/os_servicio/','/gestion/os_servicio/(:num)'), function ($num=1) {
  isLogin(); // comprueba si esta logueado

  // todos las ordenes de servcio con sus clientes y usuarios
  $os_servicio = App::Db()->query("SELECT * FROM ordenes_servicio INNER JOIN clientes ON ordenes_servicio.cliente_id = clientes.cliente_id,usuarios ON ordenes_servicio.usuario_id = usuarios.usuario_id")->fetchAll();

  // Cuenta el total
  $total = count($os_servicio);

  // limita la paginacion
  $limit = App::$config['pagination'];

  // total de los productos
  $resultadoTotal = ceil($total / $limit);
  // Encontrar el valor más alto
  $num = max($num, 1);
  // Encontrar el valor más bajo
  $num = min($num, $resultadoTotal);
  // compensar
  $offset = ($num - 1) * $limit;
  if ($offset < 0) $offset = 0;

  // extraer parte del array
  $os_servicio = array_slice($os_servicio, $offset, $limit);

  // paginacion simple
  $next = '';
  $prev = '';
  if ($num > 1)
  {
    $prev = App::Url().'/gestion/os_servicio/'.($num - 1);
  }
  if ($num < $resultadoTotal)
  {
    $next = App::Url().'/gestion/os_servicio/'.($num + 1);
  }


  $file = 'os_servicio.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});



