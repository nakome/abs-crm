<?php   defined('ACCESSO') or die('No direct script access.');

/**===================================
 *        Rutas para:
 *        Usuarios
 ====================================*/

/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/usuarios/json/get/usuario', function () {
  isLogin(); // comprueba si esta logueado
  $usuarios = App::Db()->select('usuarios','*');
  $data = array(); // iniciamos el array
  foreach ($usuarios as $usuario) {// obtenemos todos los objetos
    $data[] = $usuario;
  }
  return App::Json($data); // retorna en formato json para el javascript
});


/**
*  @todo: ruta nuevo usuario
*/
$App->Route('/gestion/usuarios/nuevo', function () {
  isLogin(); // comprueba si esta logueado
  // submit botton
  if(App::Post('saveUser')){
    // obtenemos todos los clientes
    $todos = App::Db()->select('usuarios',array('u_nombre','u_email','u_dni'));
    foreach($todos as $t){
        // si ya existe mandamos error
        if($t['u_email'] == App::Post('email')){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('email'));
          return App::Redirect(App::Url().'/gestion/usuarios/nuevo');
        }
        if($t['u_dni'] == App::Post('dni') ){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('dni'));
          return App::Redirect(App::Url().'/gestion/usuarios/nuevo');
        }
    }

    // guardamos datos si todo es correcto
    $data = array(
      'u_nombre' => ucfirst(App::Post('name')),
      'u_apellidos' => App::Post('last_name'),
      'u_dni' => App::Post('dni'),
      'u_email' => App::Post('email'),
      'u_direccion' => App::Post('address'),
      'u_ciudad' => App::Post('locality'),
      'u_password' => sha1(md5(App::Post('password'))),
      'u_observaciones' => App::Post('observations'),
      'u_tlf' => App::Post('tlf'),
      'u_role' => (App::Post('role')) ? App::Post('role') : 'empleado'
    );
   // actualizamos la funcion
    setDb('usuarios',$data,'/gestion/usuarios');
  }
  $file = 'nueva/nuevo_usuario.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});



/**
*  @todo: ruta editar usuario
*/
$App->Route('/gestion/usuarios/editar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // obtenemos el email y el dni de los demas usuarios
  $usuario = App::Db()->get('usuarios','*',array('usuario_id' => $id));
  // submit botton
  if(App::Post('editUser')){
    // obtenemos todos los clientes
    $todos = App::Db()->get('usuarios',array('u_email','u_dni'));
    foreach($todos as $t){
        // saltamos el email y el dni a editar
        if($t['u_email'] == $usuario['u_email']) continue;
        if($t['u_dni'] == $usuario['u_dni']) continue;
        // si ya existe mandamos error
        if($t['email'] == App::Post('email')){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('email'));
          return App::Redirect(App::Url().'/gestion/usuarios/editar/'.$id);
        }
        if($t['dni'] == App::Post('dni') ){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('dni'));
          return App::Redirect(App::Url().'/gestion/usuarios/editar/'.$id);
        }
    }
    // guardamos datos si todo es correcto
    $data = array(
      'u_nombre' => (App::Post('name')) ? ucFirst(App::Post('name')) : $usuario['u_nombre'],
      'u_apellidos' => (App::Post('last_name')) ? App::Post('last_name'): $usuario['u_apellidos'],
      'u_dni' => (App::Post('dni')) ? App::Post('dni') : $usuario['u_dni'],
      'u_email' => (App::Post('email')) ? App::Post('email') : $usuario['u_email'],
      'u_direccion' => (App::Post('address')) ? App::Post('address') : $usuario['u_direccion'],
      'u_ciudad' => (App::Post('locality')) ? App::Post('locality'): $usuario['u_ciudad'],
      'u_password' => (App::Post('password')) ? sha1(md5(App::Post('password'))) : $usuario['u_password'],
      'u_observaciones' => (App::Post('observations')) ? App::Post('observations') : $usuario['u_observaciones'],
      'u_tlf' => (App::Post('tlf')) ? App::Post('tlf') : $usuario['u_tlf'],
      'u_role' => (App::Post('role')) ? App::Post('role') : $usuario['u_role']
    );
    // actualizamos
    putDb('usuarios',$data,'usuario_id',$id,'/gestion/usuarios');
  }
  $file = 'editar/editar_usuario.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: ruta borrar usuario
*/
$App->Route('/gestion/usuarios/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  delDb('usuarios','usuario_id',$id,'/gestion/usuarios');
});


/**
*  @todo: ruta usuarios
*/
$App->Route('/gestion/usuarios', function () {
  isLogin(); // comprueba si esta logueado
  $usuarios = App::Db()->select('usuarios','*');// obtenemos todos los usuarios
  $file = 'usuarios.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
