<?php   defined('ACCESSO') or die('No direct script access.');

/**===================================
 *        Rutas para:
 *        Servicios
 ====================================*/

/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/servicios/json/get/servicio', function () {
  isLogin(); // comprueba si esta logueado
  $servicios = App::Db()->select('servicios','*');
  $data = array(); // iniciamos el array
  foreach ($servicios as $servicio) {// obtenemos todos los objetos
    $data[] = $servicio;
  }
  return App::Json($data); // retorna en formato json para el javascript
});


/**
*  @todo: nuevo servicio
*/
$App->Route('/gestion/servicios/nuevo', function () {
  isLogin(); // comprueba si esta logueado

  // submit botton
  if(App::Post('saveService')){
    // guardamos datos si todo es correcto
    $data = array(
      'nombre' => ucfirst(App::Post('nombre')),
      'descripccion' => App::Post('descripccion'),
      'precio' => App::Post('precio'),
      'observaciones' => App::Post('observaciones'),
      'fecha' => Date('d/m/Y'),
    );
    // si todo va bien guardamos
    setDb('servicios',$data,'/gestion/servicios');
  }

  $file = 'nueva/nuevo_servicio.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: editar servicio
*/
$App->Route('/gestion/servicios/editar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // obtenemos todos los marcas
  $servicio = App::Db()->get('servicios','*',array('servicio_id' => (int) $id));
  // submit botton
  if(App::Post('editService')){
    // guardamos datos si todo es correcto
    $update = array(
      'nombre' => (ucfirst(App::Post('nombre'))) ? App::Post('nombre') : $servicio['nombre'],
      'descripccion' => App::Post('descripccion'),
      'precio' => (App::Post('precio')) ? App::Post('precio') : $servicio['precio'],
      'observaciones' => App::Post('observaciones'),
      'fecha' => $servicio['fecha'],
    );
    // si todo va bien actualizamos
    putDb('servicios',$update,'servicio_id',App::Post('servicio_id'),'/gestion/servicios');
  }


  $file = 'editar/editar_servicio.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: borrar servicio
*/
$App->Route('/gestion/servicios/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos
  delDb('servicios','servicio_id',$id,'/gestion/servicios');
});




/**
*  @todo: servicios
*/
$App->Route(array('/gestion/servicios/','/gestion/servicios/(:num)'), function ($num=1) {
  isLogin(); // comprueba si esta logueado

  // todos los servcios
  $servicios = App::Db()->select('servicios','*');

  // Cuenta el total
  $total = count($servicios);

  // limita la paginacion
  $limit = App::$config['pagination'];

  // total de los productos
  $resultadoTotal = ceil($total / $limit);
  // Encontrar el valor más alto
  $num = max($num, 1);
  // Encontrar el valor más bajo
  $num = min($num, $resultadoTotal);
  // compensar
  $offset = ($num - 1) * $limit;
  if ($offset < 0) $offset = 0;

  // extraer parte del array
  $servicios = array_slice($servicios, $offset, $limit);


  // paginacion simple
  $next = '';
  $prev = '';
  if ($num > 1)
  {
    $prev = App::Url().'/gestion/servicios/'.($num - 1);
  }
  if ($num < $resultadoTotal)
  {
    $next = App::Url().'/gestion/servicios/'.($num + 1);
  }

  $file = 'servicios.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
