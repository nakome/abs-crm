<?php   defined('ACCESSO') or die('No direct script access.');


/**===================================
 *        Rutas para:
 *        Productos
 ====================================*/

/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/productos/json/get/producto', function () {
  isLogin(); // comprueba si esta logueado
  $productos = App::Db()->select('productos','*');
  $data = array(); // iniciamos el array
  foreach ($productos as $producto) {// obtenemos todos los objetos
    $data[] = $producto;
  }
  return App::Json($data); // retorna en formato json para el javascript
});


/**
*  @todo: nuevo producto
*/
$App->Route('/gestion/productos/nuevo', function () {
  isLogin(); // comprueba si esta logueado
  // boton submit
  if(App::Post('saveProduct')){
    // si no hay error en la foto
    if(!$_FILES['photo']['error']){
      // convertir el nombre  a minusculas
      $name = strtolower(App::Post('name'));
      // usamos el nombre foto para genere un nombre con id unico
      // ya que se guarda antes en la base de datos
      $nombreFoto = uniqid(md5(abs));
      $data = array(
        'nombre' => ucfirst($name), // primera letra en mayusculas
        'precio_compra' => App::Post('purchase_price'),
        'precio_venta' => App::Post('sales_price'),
        'stock' => App::Post('stock'),
        'stock_min' => App::Post('stock_min'),
        'observaciones' => App::Post('observations'),
        'categoria' => App::Post('category'),
        'proveedor' => (App::Post('provider')) ? App::Post('provider') :'Otro',
        'marca' => App::Post('trademark'),
        'fecha' => date('m/d/Y'),
        'imagen' => '/imagenes/productos/'.$nombreFoto.'.'.App::FileExt($_FILES['photo']["name"])
      );
      //añadimos
      $db = App::Db()->insert('productos',$data);
      if($db){
        // mandar la foto a la carpeta
        move_uploaded_file($_FILES['photo']["tmp_name"],ROOT.'/imagenes/productos/'.$nombreFoto.'.'.App::FileExt($_FILES['photo']["name"]));
        App::SetMsg(App::$lang['success'],App::$lang['the_file_has_been_added']);
        App::Redirect(App::Url().'/gestion/productos/editar/'.$db);
      }
    }else{
      App::SetMsg(App::$lang['error'],App::$lang['the_file_can_not_added']);
      App::Redirect(App::Url().'/gestion/productos/nuevo');
    }
  }
  $file = 'nueva/nuevo_producto.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: editar producto
*/
$App->Route('/gestion/productos/editar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  $producto = App::Db()->get('productos','*',array('producto_id' => $id));
  // boton submit
  if(App::Post('editProduct')){
      // convertir el nombre  a minusculas
      $name = strtolower(App::Post('name'));
      $data = array(
        'nombre' => (App::Post('name')) ? ucfirst($name) : $producto['nombre'], // primera letra en mayusculas
        'precio_compra' => (App::Post('purchase_price')) ? App::Post('purchase_price') : $producto['precio_compra'],
        'precio_venta' => (App::Post('sales_price')) ? App::Post('sales_price') : $producto['precio_venta'],
        'stock' => (App::Post('stock')) ? App::Post('stock') : $producto['stock'],
        'stock_min' => (App::Post('stock_min')) ? App::Post('stock_min') : $producto['stock_min'],
        'observaciones' => App::Post('observations'),
        'categoria' => (App::Post('category')) ? App::Post('category') : $producto['categoria'],
        'proveedor' => (App::Post('provider')) ? App::Post('provider') : $producto['proveedor'],
        'marca' => (App::Post('trademark')) ? App::Post('trademark') : $producto['marca'],
        'fecha' => date('m/d/Y'),
        'imagen' => $producto['imagen']
      );
      $categoria = (App::Post('category')) ? App::Post('category') : $producto['categoria'];
      // actualizamos la funcion
      putDb('productos',$data,'producto_id',$id,'/gestion/filtro/categoria/'.urlencode($categoria));
  }
  $file = 'editar/editar_producto.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: borrar producto
*/
$App->Route('/gestion/productos/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // buscamos la imagen
  $photo = App::Db()->select('productos',array('imagen'),array('producto_id' => $id));
  // la borramos
  @unlink(ROOT.$photo[0]['imagen']);
  // borramos en la base de datos
  App::Db()->delete('productos', array('AND' => array('producto_id'=> $id)));
  // enviamos mensaje
  App::SetMsg(App::$lang['success'],App::$lang['the_file_has_been_delete']);
  // refreshcamos
  App::Redirect(App::Url().'/gestion/productos');
});


/**
*  @todo: productos categoria
*/
$App->Route(array('/gestion/filtro/categoria/(:any)','/gestion/filtro/categoria/(:any)/(:num)'), function ($categoria,$num = 1) {
  isLogin(); // comprueba si esta logueado
  // la categoria con espacios
  $categoria = urldecode($categoria);
  // optiene todos los productos de la misma categoria
  $productos = App::Db()->select('productos','*',array('categoria' => $categoria));
  // Cuenta el total
  $total = count($productos);
  // limita la paginacion
  $limit = App::$config['pagination'];
  // total de los productos
  $resultadoTotal = ceil($total / $limit);
  // Encontrar el valor más alto
  $num = max($num, 1);
  // Encontrar el valor más bajo
  $num = min($num, $resultadoTotal);
  // compensar
  $offset = ($num - 1) * $limit;
  if ($offset < 0) $offset = 0;
  // extraer parte del array
  $productos = array_slice($productos, $offset, $limit);
  // paginacion simple
  $next = '';
  $prev = '';
  if ($num > 1)
  {
    $prev = App::Url().'/gestion/filtro/categoria/'.$categoria.'/'.($num - 1);
  }
  if ($num < $resultadoTotal)
  {
    $next = App::Url().'/gestion/filtro/categoria/'.$categoria.'/'.($num + 1);
  }
  $file = 'categorias_producto.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});

/**
*  @todo: productos
*/
$App->Route('/gestion/productos/', function () {
  isLogin(); // comprueba si esta logueado
  $file = 'productos.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
