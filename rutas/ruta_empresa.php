<?php   defined('ACCESSO') or die('No direct script access.');

/**===================================
 *        Rutas para:
 *        Empresa
 ====================================*/



/**
*  @todo: ruta empresa
*/ 
$App->Route('/gestion/empresa/', function () {
  isLogin(); // comprueba si esta logueado
  // variables para la plantilla
  $empresa = App::Db()->get('empresa','*');
  $logo = App::Url().'/imagenes/empresa/'.$empresa['logo'];
  $details = $empresa['descripccion'];
  $terms = $empresa['terminos'];

  // boton editar logo
  if(App::Post('editLogo')){
    if(!$_FILES['photo']['error']){
      $imagen = 'default.png';
      // mandar la foto a la carpeta
      move_uploaded_file($_FILES['photo']["tmp_name"],ROOT.'/imagenes/empresa/'.$imagen);
      // guardamos datos si todo es correcto
      $saveCompanyId = App::Db()->update('empresa', array(
        'logo' => ($_FILES['photo']["name"]) ? $imagen : $empresa['logo']
      ),array('empresa_id' => 1));
      // si todo va bien refrescamos pagina
      if($saveCompanyId){
        App::SetMsg(App::$lang['success'],App::$lang['the_file_has_been_update']);
        App::Redirect(App::Url().'/gestion/empresa');
      }
    }else{
      App::SetMsg(App::$lang['success'],App::$lang['the_file_can_not_added']);
      App::Redirect(App::Url().'/gestion/empresa');
    }
  }
  // boton editar compañia
  if(App::Post('editCompany')){
      // guardamos datos si todo es correcto
      $saveCompanyId = App::Db()->update('empresa', array(
        'descripccion' =>(App::Post('details')) ? ucfirst(App::Post('details')) : $empresa['descripccion'],
        'terminos' =>App::Post('terms')
      ),array('empresa_id' => 1));
      // si todo va bien refrescamos pagina
      if($saveCompanyId){
        App::SetMsg(App::$lang['success'],App::$lang['the_file_has_been_update']);
        App::Redirect(App::Url().'/gestion/empresa');
      }else{
        App::SetMsg(App::$lang['success'],App::$lang['the_file_can_not_added']);
        App::Redirect(App::Url().'/gestion/empresa');
      }
  }

  $file = 'empresa.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: ruta preview 
*/
$App->Route('/gestion/empresa/preview', function () {
  isLogin(); // comprueba si esta logueado
  $empresa = App::Db()->get('empresa','*');
  $logo = App::Url().'/imagenes/empresa/'.$empresa['logo'];
  $details = $empresa['descripccion'];
  $terms = $empresa['terminos'];
  // incluimos el archivo html
  include PLANTILLA.'/factura.html';
});
