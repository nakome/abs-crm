<?php   defined('ACCESSO') or die('No direct script access.');


/**===================================
 *        Rutas para:
 *        Clientes
 ====================================*/



/**
*  @todo: para el autocompletado retorna en formato json para leer en el javascript
*/
$App->Route('/gestion/clientes/json/get/cliente', function () {
  isLogin(); // comprueba si esta logueado
  // obtenemos todos los clientes
  $clientes = App::Db()->select('clientes','*');
  $data = array(); // iniciamos el array
  foreach ($clientes as $cliente) {// obtenemos todos los objetos
    $data[] = $cliente;
  }
  return App::Json($data); // retorna en formato json para el javascript
});


/**
*  @todo: ruta nuevo cliente
*/
$App->Route('/gestion/clientes/nuevo', function () {
  isLogin(); // comprueba si esta logueado
  // boton guardar
  if(App::Post('saveClient')){
    // obtenemos todos los clientes
    $todos = App::Db()->select('clientes',array('c_email','c_dni'));
    foreach($todos as $t){
        // si ya existe mandamos error
        if($t['c_email'] == App::Post('email')){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('email'));
          return App::Redirect(App::Url().'/gestion/clientes/nuevo');
        }
        if($t['c_dni'] == App::Post('dni') ){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('dni'));
          return App::Redirect(App::Url().'/gestion/clientes/nuevo');
        }
    }

    // guardamos datos si todo es correcto
    $data = array(
      'c_nombre' => ucfirst(App::Post('name')),
      'c_apellidos' => App::Post('last_name'),
      'c_dni' => App::Post('dni'),
      'c_email' => App::Post('email'),
      'c_direccion' => App::Post('address'),
      'c_ciudad' => App::Post('locality'),
      'c_cp' => App::Post('cp'),
      'c_observaciones' => App::Post('observations'),
      'c_tlf' => App::Post('tlf'),
      'c_fecha' => Date('d/m/Y'),
    );
    // si todo va bien refrescamos pagina
    setDb('clientes',$data,'/gestion/clientes');
  }
  $file = 'nueva/nuevo_cliente.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: ruta editar cliente
*/
$App->Route('/gestion/clientes/editar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
 // obtenemos el email y el dni de los demas usuarios
  $cliente = App::Db()->get('clientes','*',array('cliente_id' => $id));
  // submit botton
  if(App::Post('editClient')){
      // obtenemos todos los clientes
      $todos = App::Db()->select('clientes',array('c_email','c_dni'));
      foreach($todos as $t){
          // saltamos el email y el dni a editar
          if($t['c_email'] == $cliente['c_email']) continue;
          if($t['c_dni'] == $cliente['c_dni']) continue;
          // si ya existe mandamos error
          if($t['c_email'] == App::Post('email')){
            App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('email'));
            return App::Redirect(App::Url().'/gestion/clientes/editar/'.$id);
          }
          if($t['c_dni'] == App::Post('dni') ){
            App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('dni'));
            return App::Redirect(App::Url().'/gestion/clientes/editar/'.$id);
          }
      }
      // guardamos datos si todo es correcto
      $data = array(
        'c_nombre' => (App::Post('name')) ? ucfirst(App::Post('name')) : $cliente['c_nombre'],
        'c_apellidos' =>(App::Post('last_name')) ? ucfirst(App::Post('last_name')) : $cliente['c_apellidos'],
        'c_dni' => (App::Post('dni')) ? App::Post('dni') : $cliente['c_dni'],
        'c_tlf' => (App::Post('tlf')) ? App::Post('tlf') : $cliente['c_tlf'],
        'c_email' => (App::Post('email')) ? App::Post('email') : $cliente['c_email'],
        'c_direccion' => (App::Post('address')) ? App::Post('address') : $cliente['c_direccion'],
        'c_ciudad' => (App::Post('locality')) ? App::Post('locality') : $cliente['c_ciudad'],
        'c_cp' => (App::Post('cp')) ? App::Post('cp') : $cliente['c_cp'],
        'c_observaciones' =>(App::Post('observations')) ? App::Post('observations') : $cliente['c_observaciones'],
        'c_fecha' => $cliente['c_fecha'],
      );
      // actualizamos la funcion
      putDb('clientes',$data,'cliente_id',$id,'/gestion/clientes');
  }

  $file = 'editar/editar_cliente.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: ruta borrar cliente
*/
$App->Route('/gestion/clientes/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos
  delDb('clientes','cliente_id',$id,'/gestion/clientes');
});


/**
*  @todo: ruta clientes
*/
$App->Route(array('/gestion/clientes','/gestion/clientes/(:num)'), function ($num=1) {
  isLogin(); // comprueba si esta logueado
  // obtenemos todos los clientes
  $clientes = App::Db()->select('clientes','*');
  // Cuenta el total
  $total = count($clientes);
  // limita la paginacion
  $limit = App::$config['pagination'];
  // total de los productos
  $resultadoTotal = ceil($total / $limit);
  // Encontrar el valor más alto
  $num = max($num, 1);
  // Encontrar el valor más bajo
  $num = min($num, $resultadoTotal);
  // compensar
  $offset = ($num - 1) * $limit;
  if ($offset < 0) $offset = 0;
  // extraer parte del array
  $clientes = array_slice($clientes, $offset, $limit);
  // paginacion simple
  $next = '';
  $prev = '';
  if ($num > 1)
  {
    $prev = App::Url().'/gestion/clientes/'.($num - 1);
  }
  if ($num < $resultadoTotal)
  {
    $next = App::Url().'/gestion/clientes/'.($num + 1);
  }


  $file = 'clientes.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
