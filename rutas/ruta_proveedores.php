<?php   defined('ACCESSO') or die('No direct script access.');

/**===================================
 *        Rutas para:
 *        Proveedores
 ====================================*/

/**
*  @todo: ruta borrar proveedores
*/ 
$App->Route('/gestion/proveedores/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos row
  delDb('proveedores','proveedor_id',$id,'/gestion/proveedores');
});

/**
*  @todo: proveedores editar y guardar tambien van aqui
*/ 
$App->Route('/gestion/proveedores/', function () {
  isLogin(); // comprueba si esta logueado
  // añadir proveedor
  if(App::Post('addProvider')){
    $data = array(
      'nombre' => App::Post('name'),
      'direccion' => App::Post('address'),
      'tlf' => App::Post('tlf'),
      'email' => App::Post('email'),
      'observaciones' => App::Post('observations'),
      'fecha' => date('d/m/Y')
    );
    // guardamos array y redirigimos
    setDb('proveedores',$data,'/gestion/proveedores');
  }
  // editar proveedor
  if(App::Post('editProvider')){
    $id = App::Post('proveedor_id');
    $data = array(
      'nombre' => App::Post('name'),
      'direccion' => App::Post('address'),
      'tlf' => App::Post('tlf'),
      'email' => App::Post('email'),
      'observaciones' => App::Post('observations'),
      'fecha' => date('d/m/Y')
    );
    // guardamos array y redirigimos
    putDb('proveedores',$data,'proveedor_id',$id,'/gestion/proveedores');
  }

  $file = 'proveedores.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
