<?php   defined('ACCESSO') or die('No direct script access.');

/**===================================
 *        Rutas para:
 *        Marcas
 ====================================*/



/**
*  @todo: ruta borrar categoria
*/ 
$App->Route('/gestion/marcas/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos la categoria
  delDb('marcas','marca_id',$id,'/gestion/marcas');
});


/**
*  @todo: ruta marcas
*/ 
$App->Route(array('/gestion/marcas/','/gestion/marcas/(:num)'), function ($num = 1) {
  isLogin(); // comprueba si esta logueado
  // submit botton
  if(App::Post('editTrademark')){
    // obtenemos todos los marcas
    $trademark = App::Db()->get('marcas','*',array('marca_id' => (int) App::Post('id')));
    // guardamos datos si todo es correcto
    $update = array(
      'nombre' => (ucfirst(App::Post('name'))) ? App::Post('name') : $trademark['nombre'],
      'descripccion' => App::Post('description')
    );
    // si todo va bien refrescamos pagina
    putDb('marcas',$update,'marca_id',App::Post('id'),'/gestion/marcas');
  }

  // submit botton
  if(App::Post('addTrademark')){
    // guardamos datos si todo es correcto
    $new = array(
      'nombre' => ucfirst(App::Post('name')),
      'descripccion' => App::Post('description')
    );
    // si todo va bien refrescamos pagina
    setDb('marcas',$new,'/gestion/marcas');
  }

  // todas las marcas
  $marcas = App::Db()->select('marcas','*');
  $file = 'marcas.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
