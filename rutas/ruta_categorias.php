<?php   defined('ACCESSO') or die('No direct script access.');


/**===================================
 *        Rutas para:
 *        Categorias de Producto
 ====================================*/

/**
*  @todo: ruta borrar categoria
*/
$App->Route('/gestion/categorias/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos la categoria
  delDb('categorias','categoria_id',$id,'/gestion/categorias');
});


/**
*  @todo: ruta categorias
*/
$App->Route('/gestion/categorias/', function () {
  isLogin(); // comprueba si esta logueado

  // boton editar categoria
  if(App::Post('editCategory')){
    // obtenemos todos los categorias
    $categoria = App::Db()->get('categorias','*',array('categoria_id' => (int) App::Post('id')));
    // guardamos datos si todo es correcto
    $data = array(
      'nombre' => (ucfirst(App::Post('name'))) ? App::Post('name') : $categoria['nombre'],
      'modelo' => (ucfirst(App::Post('model'))) ? App::Post('model') : $categoria['modelo'],
      'descripccion' => App::Post('description')
    );
    // si todo va bien refrescamos pagina
    putDb('categorias',$data,'categoria_id',App::Post('id'),'/gestion/categorias');
  }

  // boton añadir categoria
  if(App::Post('addCategory')){
    // obtenemos todos las categorias
    $all = App::Db()->select('categorias',array('modelo'));
    foreach($all as $t){
        // si ya existe mandamos error
        if($t['modelo'] == App::Post('model')){
          App::SetMsg(App::$lang['error'],App::$lang['data_exists'].' '.App::Post('model'));
          return App::Redirect(App::Url().'/gestion/categorias');
        }
    }
    // guardamos datos si todo es correcto
    $data = array(
      'nombre' => ucfirst(App::Post('name')),
      'modelo' => ucfirst(App::Post('model')),
      'descripccion' => App::Post('description')
    );
    // si todo va bien refrescamos pagina
    setDb('categorias',$data,'/gestion/categorias');
  }

  $file = 'categorias.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
