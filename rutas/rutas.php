<?php defined('ACCESSO') or die('No direct script access.');


/**===================================
 *               Rutas
 ====================================*/

/**
*  @todo: Iniciamos la App
*/
$App = new App();

/**
*  @todo: obtenemos la key en formato json
*/
$App->Route('/gestion/comprobar/(:any)/(:any)', function ($db,$key) {
  $database = App::Db()->select($db,array($key));
  return App::Json($database);
});



/**
*  @todo: Incluimos las rutas separadas en archivos
*/
include RUTAS.'/ruta_notas.php';
include RUTAS.'/ruta_categorias.php';
include RUTAS.'/ruta_clientes.php';
include RUTAS.'/ruta_empresa.php';
include RUTAS.'/ruta_listacompra.php';
include RUTAS.'/ruta_marcas.php';
include RUTAS.'/ruta_ordenes_servicio.php';
include RUTAS.'/ruta_productos.php';
include RUTAS.'/ruta_proveedores.php';
include RUTAS.'/ruta_servicios.php';
include RUTAS.'/ruta_usuarios.php';


/**
*  @todo: Crear una copia de la base de datos guardada en Backup
*/
$App->Route('make/backup', function() {
    isLogin();// check login
    // guardamos una copia en backup
    copy(DATABASE.'/db.sqlite', BACKUP.'/db.sqlite');
    sleep(1); // esperamos 1s
    // zip imagenes
    App::Zip('imagenes',IMAGENES,BACKUP);
    sleep(1);// esperamos 1s
    // copiamos el archivo imagenes
    copy(ROOT.'/imagenes.zip', BACKUP.'/imagenes.zip');
    sleep(1);// esperamos 1s
    // borramos el zip del root
    @unlink(ROOT.'/imagenes.zip');
    sleep(1);// esperamos 1s
    // send msg
    App::SetMsg(App::$lang['success'],App::$lang['valid_backup']);
    // abrimos ventana para guardar
    App::Redirect(App::Url());
});


/**
*  @todo: Generar password
*/
$App->Route('/generar/password/(:any)', function ($password) {
  echo 'Passwrord: '.urldecode($password).'<br>Passwrord encriptado: '.sha1(md5($password));
});

/**
*  @todo: Cambiar lenguaje
*/
$App->Route('/lenguaje/(:any)', function ($lenguaje) {
  App::SessionSet('lenguaje',trim($lenguaje));
  App::Redirect(App::Url());
});

/**
*  @todo: Entrar
*/
$App->Route('/entrar', function () {

  // comprueba el logueo
  if (App::SessionGet('app_uid') && App::SessionGet('app_email')){
    // si es correcto entramos en la gestion
    App::Redirect(App::Url());
  }else{
    //  formulario
    if(App::Post('auth')){
      $email = App::Post('email');
      $password = App::Post('password');
      // comprobamos si el correo es valido
      if(App::ValidEmail($email) == false){
        // Si no es valido lanzamos el mensaje
        App::SetMsg(App::$lang['error'], App::$lang['invalid_email']);
        App::Redirect(App::Url().'/entrar'); // redirigimos a entrar
      }else{
        // Comprobamos si esta en la base de datos
        if (App::Db()->has('usuarios', array('AND' => array( 'OR' => array( 'u_email' => $email),"u_password" => sha1(md5($password)))))){
          $d = App::Db()->get('usuarios', array('u_role','usuario_id'), array('u_email' => $email));
          // si es valido agregamos sessiones
          App::SessionSet('app_uid',App::GenerateToken());
          App::SessionSet('app_email',$email);
          App::SessionSet('app_id',$d['usuario_id']);
          App::SessionSet('app_role',$d['u_role']);
          App::SetMsg(App::$lang['success'], App::$lang['valid_email']);
          App::Redirect(App::Url()); // vamos a la seccion gestion
        }else{
          // Contraseña incorrecta
          App::SetMsg(App::$lang['error'],App::$lang['invalid_password']);
          App::Redirect(App::Url().'/entrar'); // redirigimos a entrar
        }
      }
    }// fin formulario
    // incluimos el archivo html
    include PLANTILLA.'/index.html';
  }
});

/**
*  @todo: Salir
*/
$App->Route('/salir', function () {
  // destruir las sessiones y volver al inicio
  App::SessionDelete('app_uid');
  App::SessionDelete('app_email');
  App::SessionDestroy();
  App::Redirect(App::Url());
  die();
});


/**
*  @todo: inicio
*/
$App->Route('/', function () {
  isLogin(); // comprueba si esta logueado
  $file = 'dashboard.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


/**
*  @todo: lanzamos la aplicacion
*/
$App->launch();


