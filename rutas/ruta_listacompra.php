<?php   defined('ACCESSO') or die('No direct script access.');


/**===================================
 *        Rutas para:
 *        Lista Compra
 ====================================*/



/**
*  @todo: ruta de ajax para actualizar stock
*/
$App->Route('/gestion/lista_compra/actualizar/stock/producto/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  $_POST = json_decode(file_get_contents('php://input'), true); // recibe los POST en formato json
  $nuevoStock = App::Post('stock'); // recibimos el { stock } en formato array
  // actualizamos
  $db = App::Db()->update('productos',array('stock' => $nuevoStock),array('producto_id' => $id));
  // devuelve en formato json
  return App::Json(array('estado' => true,'debug' => $nuevoStock));
});

/**
*  @todo: ruta de ajax para sumar stock basicamente es igual que actualizar stock solo que ahora suma
*/
$App->Route('/gestion/lista_compra/sumar/stock/producto/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  $_POST = json_decode(file_get_contents('php://input'), true);
  $nuevoStock = App::Post('stock');
  $db = App::Db()->update('productos',array('stock[+]' => $nuevoStock),array('producto_id' => $id));
  return App::Json(array('estado' => true,'debug' => $nuevoStock));
});


/**
*  @todo: ruta para finalizar la lista
*/
$App->Route('/gestion/lista_compra/finalizar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  $data = array('estado' => 'listo'); // mandamos el string listo
  // guardamos
  putDb('lista_compra',$data,'lista_id',$id,'/gestion/listas_compra');
});



/**
*  @todo: ruta para el autocompletado del input search
*/
$App->Route('/gestion/listas_compra/json/get/lista', function () {
  isLogin(); // comprueba si esta logueado
  // obtenemos las listas
  $listas = App::Db()->select('lista_compra','*');
  $data = array(); // iniciamos el array
  foreach ($listas as $lista) {// obtenemos todos los objetos
    $data[] = array(
      'lista_id' => $lista['lista_id'],
      'nombre' => $lista['nombre'],
      'fecha_compra' => $lista['fecha_compra'],
      'productos' => @unserialize($lista['productos']),
      'estado' => $lista['estado'],
    );
  }
  return App::Json($data); // retorna en formato json para el javascript
});



/**
*  @todo: ruta de ajax para añadir a la lista otro producto que tenga stock
*/
$App->Route('/gestion/lista_compra/add/product/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  $_POST = json_decode(file_get_contents('php://input'), true);

  // obtenemos la lista
  $lista_compra = App::Db()->get('lista_compra','*',array('lista_id' => $id));
  // sacamos los productos y los convertimos en array
  $result = unserialize($lista_compra['productos']);
  // añadimos el nuevo
  $result[] = array(
    'producto_id' => App::Post('producto_id'),
    'nombre' => App::Post('nombre'),
    'oldStock' => App::Post('stock'),
    'stock' => '1',
  );
  // lo añadimos en el array a editar
  $data = array(
    'nombre' => $lista_compra['nombre'],
    'fecha_compra' => $lista_compra['fecha_compra'],
    'productos' => @serialize($result), // este
    'estado' => $lista_compra['estado'],
  );
  // actualizamos y mandamos el estado a true para que refresque
  $db = App::Db()->update('lista_compra',$data,array('lista_id' => $id));
  if($db){
    return App::Json(array('estado' => true));
  }
});



/**
*  @todo: ruta nueva lista
*/
$App->Route('/gestion/lista_compra/nuevo', function () {
  isLogin(); // comprueba si esta logueado
  // obtenemos los productos con stock minimo
  $productosSinStock = App::Db()->query("SELECT producto_id,nombre,stock,stock_min FROM productos WHERE stock <= stock_min OR stock == 0")->fetchAll();
  $addNewObject = array();
  foreach ($productosSinStock as $item) {
    $addNewObject[] = array(
      'producto_id' =>$item['producto_id'],
      'nombre' =>$item['nombre'],
      'stock' =>$item['stock'],
      'stock_min' =>$item['stock_min'],
      'oldStock' =>$item['stock']
    );
  }

  // boton guardar lista
  if(App::Post('saveList')){
      $data = array(
        'nombre' => App::Post('nombre'),
        'fecha_compra' => (App::Post('fecha_compra')) ? App::Post('fecha_compra') : date('m/d/Y'),
        'productos' => @serialize($addNewObject),
        'estado' => 'En espera'
      );
    // si todo va  guardamos y redirecionamos a la seccion editar
    $lastId = App::Db()->insert('lista_compra',$data);
    if($lastId){
      // mandamos mensaje
      App::SetMsg(App::$lang['success'],App::$lang['the_file_has_been_added']);
      // redirijimos a el nuevo id
      App::Redirect(App::Url().'/gestion/lista_compra/editar/'.$lastId);
    }
  }
  $file = 'nueva/nueva_lista_compra.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});


// ruta editar lista_compra
$App->Route('/gestion/lista_compra/editar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // obtenemos la lista
  $lista_compra = App::Db()->get('lista_compra','*',array('lista_id' => $id));
  // boton editar lista
  if(App::Post('actualizarLista')){
      $json = json_decode(App::Post('productos'),true);
      $data = array(
        'nombre' => (App::Post('nombre')) ? App::Post('nombre') : $lista_compra['nombre'],
        'fecha_compra' => (App::Post('fecha_compra')) ? App::Post('fecha_compra') : $lista_compra['fecha_compra'],
        'productos' => @serialize($json),
        'estado' => ($lista_compra['estado'] == 'listo') ? 'listo' : 'En espera',
      );
      // actualizamos
     putDb('lista_compra',$data,'lista_id',$id,'/gestion/listas_compra');
  }

  $file = 'editar/editar_lista_compra.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});





// ruta editar lista_compra
$App->Route('/gestion/preview/lista_compra/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // obtenemos la lista
  $lista_compra = App::Db()->get('lista_compra','*',array('lista_id' => $id));
  // logo empresa etc
  $empresa = App::Db()->get('empresa','*');
  $logo = App::Url().'/imagenes/empresa/'.$empresa['logo'];
  // remplazamos \n por <br> = salto de linea
  $details = str_replace("\n",'<br>',$empresa['descripccion']);
  $terms = $empresa['terminos'];
  // incluimos el archivo html
  include PLANTILLA.'/preview_lista_compra.html';
});

// ruta borrar lista_compra
$App->Route('/gestion/lista_compra/borrar/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos lista
  delDb('lista_compra','lista_id',$id,'/gestion/listas_compra');
});


// ruta lista compra
$App->Route(array('/gestion/listas_compra/','/gestion/listas_compra/(:num)'), function ($num=1) {
  isLogin(); // comprueba si esta logueado
  // todas las listas
  $lista = App::Db()->select('lista_compra','*');
  // Cuenta el total
  $total = count($lista);
  // limita la paginacion
  $limit = App::$config['pagination'];
  // total de los productos
  $resultadoTotal = ceil($total / $limit);
  // Encontrar el valor más alto
  $num = max($num, 1);
  // Encontrar el valor más bajo
  $num = min($num, $resultadoTotal);
  // compensar
  $offset = ($num - 1) * $limit;
  if ($offset < 0) $offset = 0;
  // extraer parte del array
  $lista = array_slice($lista, $offset, $limit);
  // paginacion simple
  $next = '';
  $prev = '';
  if ($num > 1) // mayor de 1
  {
    $prev = App::Url().'/gestion/listas_compra/'.($num - 1);
  }
  if ($num < $resultadoTotal) // menor que el total
  {
    $next = App::Url().'/gestion/listas_compra/'.($num + 1);
  }

  $file = 'listas_compra.html'; // incluimos el archivo alojado en rutas_html
  // incluimos el archivo html
  include PLANTILLA.'/gestion.html';
});
