<?php   defined('ACCESSO') or die('No direct script access.');

/**===================================
 *        Rutas para:
 *        Notas
 ====================================*/



/**
*  @todo: Cambiamos la nota a true o false
*/ 
$App->Route('/gestion/notas/visto/id/(:num)/estado/(:num)', function ($id,$visto) {
  isLogin(); // comprueba si esta logueado
  $estado = '';
  if($visto == (int) 1){$estado = (int) 0;} // si true poner false
  elseif($visto == (int) 0){$estado = (int) 1;} // si false poner true
  // actualizamos
  App::Db()->update('notas', array('visto' => $estado), array('notas_id' => $id));
  // redirigimos
  return App::Redirect(App::Url());
});

/**
*  @todo: Borramos la nota
*/ 
$App->Route('/gestion/notas/borrar/id/(:num)', function ($id) {
  isLogin(); // comprueba si esta logueado
  // borramos
  App::Db()->delete('notas', array('AND' => array('notas_id' => $id)));
  // redirigimos
  return App::Redirect(App::Url());
});

