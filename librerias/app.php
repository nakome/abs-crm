<?php

  defined('ACCESSO') or die('No direct script access.');

/**
 * Functions to make routes
 *
 *  <pre>
 *    <code>
 *    $app =  new App();
 *    // go to http://localhost/foo
 *    $app->Route('/foo',function(){
 *      echo 'I am in foo';
 *    });
 *
 *    // go to http://localhost/foo/1
 *    $app->Route('/foo/(:num)',function($num){
 *      echo 'I am in foo '.$num;
 *    });
 *
 *    // go to http://localhost/foo/bar
 *    $app->Route('/foo/(:any)',function($any){
 *      echo 'I am in foo '.$any;
 *    });
 *    </code>
 *  </pre>
 *
 *
 * @author Lorena Gomez Gonzalez  <mayantigo@gmail.com>
 * @version 1.0.0
 */
class App
{

  /**
  * App name
  * @var string
  */
  const APPNAME = 'App';
  /**
  * version
  * @var string
  */
  const VERSION = '1.0.0';
  /**
  * routes array
  *
  * @var array
  */
  private $routes = array();
  /**
  * config
  *
  * @var array
  */
  public static $config = array();
  /**
  * language
  *
  * @var array
  */
  public static $lang = array();
  /**
  * actions array
  *
  * @var array
  */
  private static $actions = array();
  /**
  * token
  *
  * @var array
  */
  protected static $security_token_name = 'appcms_security_token';

  /**
  * Get config
  *
  *  <code>
  *    $this->LoadConfig();
  *  </code>
  *
  *  @access  protected
  */
  protected function LoadConfig()
  {
      if (file_exists(CONFIGURACION.'/config.php')) {
          static::$config = require CONFIGURACION.'/config.php';
      } else {
          die('Oops.. Where is config file ?!');
      }
  }
  /**
   *   Get config
   *
   *   <code>
   *     $this->LoadLanguage();
   *   </code>
   *
   *   @access  protected
   */
  protected function LoadLanguage()
  {
      if (self::SessionExists('lenguaje')) {
          if (file_exists(LENGUAJES.'/'.self::$config['lenguaje'].'.php')) {
              static::$lang = require LENGUAJES.'/'.self::SessionGet('lenguaje').'.php';
          } else {
              die('Oops.. Where is language file ?!');
          }
      } else {
          if (file_exists(LENGUAJES.'/'.self::$config['lenguaje'].'.php')) {
              static::$lang = require LENGUAJES.'/'.self::$config['lenguaje'].'.php';
          } else {
              die('Oops.. Where is language file ?!');
          }
      }
  }
  /**
   *   Get Notifications
   *
   *   <code>
   *     App::getMsg();
   *   </code>
   *
   *   @access  public
   */
  public static function getMsg()
  {
      //Top of file
    if (self::SessionGet('msg')) {
        $msg = self::SessionGet('msg');
        self::SessionDelete('msg');
    }
      if (isset($msg)) {
          echo ' <div id="notificacion" class="bg-5"><h3>'.$msg['title'].'</h3><p>'.$msg['msg'].'</p></div><script type="text/javascript">var w = setTimeout(function(){clearTimeout(w);document.getElementById("notificacion").remove();},3000);</script>';
      }
  }

  /**
   * Redirect to another pate.
   *
   * <code>
   *   App::setMsg('Success','data saved!');
   * </code>
   *
   * @access  public
   *
   * @param string $title  title
   * @param string $msg  message
   */
  public static function setMsg($title, $msg)
  {
      $data = array(
      'title' => $title,
      'msg' => $msg,
    );
      self::SessionSet('msg', $data);
  }

  /**
   *  Init database
   *
   *  <code>
   *    $this->Db();
   *  </code>
   *
   *  @access  public
   */
  public static function Db()
  {
      $mysql = self::$config['mysql'];
      $sqlite = self::$config['sqlite'];
      $db = '';
      if (self::$config['base_de_datos'] == 'sqlite') {
          $db = new medoo($sqlite);
      } else {
          $db = new medoo($mysql);
      }

      return $db;
  }

  /**
   *  Check valid mail
   *
   *  <code>
   *    App::ValidEmail('jshon@gmail.com')
   *  </code>
   *
   *  @access  public
   *
   *  @param string $email  email
   */
  public static function ValidEmail($email)
  {
      return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) ? false : true;
  }

  /**
   *  Post data
   *
   *  <code>
   *    App::Post($key); = $_POST['KEY'];
   *  </code>
   *
   *  @access  public
   *
   *  @param string $key  input name
   */
  public static function Post($key)
  {
      return self::ArrGet($_POST, $key);
  }
  /**
   *  Get data
   *
   *  <code>
   *    default App::Get($key); = $_GET['KEY'];
   *  </code>
   *
   *  @access  public
   *
   *  @param string $key  input name
   */
  public static function Get($key)
  {
      return self::ArrGet($_GET, $key);
  }

  /**
   *  Clear tags in file
   *
   *  <code>
   *    default App::cleanString();
   *  </code>
   *
   *  @access  public
   *
   *  @param string $str  html
   */
  public static function cleanString($str)
  {
      return htmlspecialchars($str, ENT_QUOTES, 'utf-8');
  }
  /**
   *  Convert array to json
   *
   *  <code>
   *    App::Json($array);
   *  </code>
   *
   *  @access  public
   *
   *  @param array $array   array to convert
   */
  public static function Json($array)
  {
      // set headers
    @header('Content-Type: application/json');
    // print json
    return print_r(json_encode($array));
  }

  /**
   *  Scan directories
   *
   *  <code>
   *    App::ScanDir('blog');
   *  </code>
   *
   *  @access  public
   *
   *  @param string $dir  dir to scan
   */
  public static function ScanDir($dir)
  {
      // Redefine vars
    $dir = (string) $dir;
    // Scan dir
    if (is_dir($dir) && $dh = opendir($dir)) {
        $f = array();
        while ($fn = readdir($dh)) {
            if ($fn != '.' && $fn != '..' && is_dir($dir.'/'.$fn)) {
                $f[] = $fn;
            }
        }

        return $f;
    }
  }
  /**
   *  Get extension of file
   *
   *  <code>
   *    App::FileExt('file.txt');
   *  </code>
   *
   *  @access  public
   *
   *  @param string $file   extension file
   */
  public static function FileExt($file)
  {
    // Redefine vars
    $file = (string) $file;
    // Return file extension
    return substr(strrchr($file, '.'), 1);
  }

  /**
   *  Get filename
   *
   *  <code>
   *    App::FileName('file.txt');
   *  </code>
   *
   *  @access  public
   *
   *  @param string $file   archive name
   */
  public static function FileName($file)
  {
      // Redefine vars
    $file = (string) $file;
    // Return filename
    return basename($file, '.'.self::FileExt($file));
  }

  /**
   *  Scan folders
   *
   *  <code>
   *    App::FileScan('file.txt',array('txt','md'));
   *  </code>
   *
   *  @access  public
   *
   *  @param string $folder   carpeta
   *  @param string $type   tipo
   *  @param bolean $file_path  other folders ?
   */
  public static function FileScan($folder, $type = null, $file_path = true)
  {
      $data = array();
      if (is_dir($folder)) {
          $iterator = new RecursiveDirectoryIterator($folder);
          foreach (new RecursiveIteratorIterator($iterator) as $file) {
              if ($type !== null) {
                  if (is_array($type)) {
                      $file_ext = substr(strrchr($file->getFilename(), '.'), 1);
                      if (in_array($file_ext, $type)) {
                          if (strpos($file->getFilename(), $file_ext, 1)) {
                              if ($file_path) {
                                  $data[] = $file->getPathName();
                              } else {
                                  $data[] = $file->getFilename();
                              }
                          }
                      }
                  } else {
                      if (strpos($file->getFilename(), $type, 1)) {
                          if ($file_path) {
                              $data[] = $file->getPathName();
                          } else {
                              $data[] = $file->getFilename();
                          }
                      }
                  }
              } else {
                  if ($file->getFilename() !== '.' && $file->getFilename() !== '..') {
                      if ($file_path) {
                          $data[] = $file->getPathName();
                      } else {
                          $data[] = $file->getFilename();
                      }
                  }
              }
          }

          return $data;
      } else {
          return false;
      }
  }
  /**
   *  Check file if exists
   *
   *  <code>
   *    App::FileExists('file.txt');
   *  </code>
   *
   *  @access  public
   *
   *  @param string $filename   archive name
   */
  public static function FileExists($filename)
  {
    // Redefine vars
    $filename = (string) $filename;
    // Return if exists and if is file
    return file_exists($filename) && is_file($filename);
  }
  /**
   *  Get files
   *
   *  <code>
   *    App::Url();
   *  </code>
   *
   *  @access  public
   *
   *  @param string $file   archive name
   */
  public static function FileGetContent($file)
  {
      // Redefine vars
    $file = (string) $file;
    // If file exists load it
    if (file_exists($file)) {
        return file_get_contents($file);
    }
  }

  /**
   *  Get base url
   *
   *  <code>
   *    App::Url();
   *  </code>
   *
   *  @access  public
   */
  public static function Url()
  {
      $https = (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') ? 'https://' : 'http://';

      return $https.rtrim(rtrim($_SERVER['HTTP_HOST'], '\\/').dirname($_SERVER['PHP_SELF']), '\\/');
  }
  /**
   *  Get base url
   *
   *  <code>
   *    App::CurrentUrl();
   *  </code>
   *
  *   @access  public
   */
  public static function CurrentUrl()
  {
      return (!empty($_SERVER['HTTPS'])) ? 'https://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
  }
  /**
   *  Get uri segments in array
   *
   *  <code>
   *    default App::getUriSegments()
   *  </code>
   *
   *  @access  public
   *
   */
  public static function getUriSegments()
  {
      return explode('/', "$_SERVER[REQUEST_URI]");
  }

  /**
   *  Get uri segment
   *
   *  <code>
   *    default App::getUri('blog')
   *  </code>
   *
   *  @access  public
   *
   *  @param string $segment  link
   */
  public static function getUri($segment)
  {
      $segments = self::getUriSegments();

      return isset($segments[$segment]) ? $segments[$segment] : null;
  }
  /**
   *  Redirect to another pate
   *
   *  <code>
   *    App::Redirect('foo.html',302,1);
   *  </code>
   *
   *  @access  public
   *
   *  @param string $url link
   *  @param number $st 302
   *  @param number $delay  number
   */
  public static function Redirect($url, $st = 302, $delay = null)
  {
      // Redefine vars
    $url = (string) $url;
      $st = (int) $st;
    // Status codes
    $msg = array();
      $msg[301] = '301 Moved Permanently';
      $msg[302] = '302 Found';
    // Is Headers sent ?
    if (headers_sent()) {
        echo "<script>document.location.href='".$url."';</script>\n";
    } else {
        // Set header
      header('HTTP/1.1 '.$st.' '.self::ArrGet($msg, $st, 302));
      // Delay execution
      if ($delay !== null) {
          sleep((int) $delay);
      }
        header("Location: $url");
      // Shutdown request
      exit(0);
    }
  }
  /**
   *  Sanitize Pretty url for SEO
   *
   *  <code>
   *    App::ParseUrl('this is a new world'); = this-is-a-new-worl
   *  </code>
   *
   *  @access  public
   *
   *  @param string $str  url
   */
  public static function ParseUrl($str)
  {
      $str = trim($str);
      $str = rawurldecode($str);
      $str = str_replace(array('--', '&quot;', '!', '@', '#', '$', '%', '^', '*', '(', ')', '+', '{', '}', '|', ':', '"', '<', '>',
                             '[', ']', '\\', ';', "'", ',', '*', '+', '~', '`', 'laquo', 'raquo', ']>', '&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8211;', '&#8212;', ),
                       array('-', '-', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
                       $str);
      $str = str_replace('--', '-', $str);
      $str = rtrim($str, '-');
      $str = str_replace('..', '', $str);
      $str = str_replace('//', '', $str);
      $str = preg_replace('/^\//', '', $str);
      $str = preg_replace('/^\./', '', $str);
    // conver space to -
    $str = trim($str);
      $str = preg_replace('/\s+/', '-', $str);

      return strtolower($str);
  }
  /**
   *  Sanitize Url
   *
   *  <code>
   *    App::sanitizeURL('url');
   *  </code>
   *
   *  @access  public
   *
   *  @param string $url  url
   */
  public static function sanitizeURL($url)
  {
      $url = trim($url);
      $url = rawurldecode($url);
      $url = str_replace(array('--', '&quot;', '!', '@', '#', '$', '%', '^', '*', '(', ')', '+', '{', '}', '|', ':', '"', '<', '>',
                             '[', ']', '\\', ';', "'", ',', '*', '+', '~', '`', 'laquo', 'raquo', ']>', '&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8211;', '&#8212;', ),
                       array('-', '-', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
                       $url);
      $url = str_replace('--', '-', $url);
      $url = rtrim($url, '-');
      $url = str_replace('..', '', $url);
      $url = str_replace('//', '', $url);
      $url = preg_replace('/^\//', '', $url);
      $url = preg_replace('/^\./', '', $url);

      return $url;
  }
    public static function runSanitizeURL()
    {
        $_GET = array_map('App::sanitizeURL', $_GET);
    }
  /**
   *  Generate token
   *
   *  <code>
   *    App::GenerateToken();
   *  </code>
   *
   *  @access  public
   *
   *  @param bolean $new false
   *  @return bolean
   */
  public static function GenerateToken($new = false)
  {
      // Get the current token
    if (isset($_SESSION[(string) self::$security_token_name])) {
        $token = $_SESSION[(string) self::$security_token_name];
    } else {
        $token = null;
    }
    // Create a new unique token
    if ($new === true or !$token) {
        // Generate a new unique token
      $token = sha1(uniqid(mt_rand(), true));
      // Store the new token
      $_SESSION[(string) self::$security_token_name] = $token;
    }
    // Return token
    return $token;
  }
  /**
   *  Check token
   *
   *  <code>
   *    App::CheckToken($t);
   *  </code>
   *
   *  @access  public
   *
   *  @param string $r  token
   *  @return bolean
   */
  public static function CheckToken($t)
  {
      return self::generateToken() === $t;
  }
  /**
   *  Set array
   *
   *  <code>
   *    App::ArrSet($array,'foo.title','value');
   *  </code>
   *
   *  @access  public
   *
   *  @param array $array   array
   *  @param string $path   path to array
   *  @param string $value  value to array
   */
  public static function ArrSet(&$array, $path, $value)
  {
      // Get segments from path
    $segments = explode('.', $path);
    // Loop through segments
    while (count($segments) > 1) {
        $segment = array_shift($segments);
        if (!isset($array[$segment]) || !is_array($array[$segment])) {
            $array[$segment] = [];
        }
        $array = &$array[$segment];
    }
      $array[array_shift($segments)] = $value;
  }
  /**
  *  Short array values
  *
  *  <code>
  *    App::ArrShort($array,'date','DESC');
  *  </code>
  *
  *  @access  public
  *
  *  @param array $a  array
  *  @param array $subkey   array
  *  @param array $order  null
  *  @return value
  */
  public static function ArrShort($a, $subkey, $order = null)
  {
      if (count($a) != 0 || (!empty($a))) {
          foreach ($a as $k => $v) {
              $b[$k] = function_exists('mb_strtolower') ? mb_strtolower($v[$subkey]) : strtolower($v[$subkey]);
          }
          if ($order == null || $order == 'ASC') {
              asort($b);
          } elseif ($order == 'DESC') {
              arsort($b);
          }
          foreach ($b as $key => $val) {
              $c[] = $a[$key];
          }

          return $c;
      }
  }
  /**
   *  Get array data
   *
   *  <code>
   *    App::ArrGet($array,'foo.title');
   *  </code>
   *
   *  @access  public
   *
   *  @param array $array   array
   *  @param array $path  path to array
   *  @param string $default  null
   *  @return array
   */
  public static function ArrGet($array, $path, $default = null)
  {
      // Get segments from path
    $segments = explode('.', $path);
    // Loop through segments
    foreach ($segments as $segment) {
        // Check
      if (!is_array($array) || !isset($array[$segment])) {
          return $default;
      }
      // Write
      $array = $array[$segment];
    }
    // Return
    return $array;
  }

  /**
   *  Render Assets
   *  <code>
   *    $App->Route('/foo/(:any)',function($any){
   *      echo $any;
   *    });
   *  </code>
   *  @access  public
   *
   *  @param array $patterns  array
   *  @param array $callback  function
   */
  public function Route($patterns, $callback)
  {
      // if not in array
    if (!is_array($patterns)) {
        $patterns = array($patterns);
    }
      foreach ($patterns as $pattern) {
          $pattern = trim($pattern, '/');
      // get any num all
      $pattern = str_replace(array('\(', '\)', '\|', '\:any', '\:num', '\:all', '#'), array('(', ')', '|', '[^/]+', '\d+', '.*?', '\#'), preg_quote($pattern, '/'));
      // this pattern
      $this->routes['#^'.$pattern.'$#'] = $callback;
      }
  }

  /**
   *  Start session
   *
   *  <code>
   *    App::SessionStart();
   *  </code>
   *
   *  @access  public
   */
  public static function SessionStart()
  {
      // Is session already started?
    if (!session_id()) {
        // Start the session
      return @session_start();
    }
    // If already started
    return true;
  }

  /**
   *  Remove session
   *
   *  <code>
   *    App::SessionDelete();
   *  </code>
   *
   *  @access  public
   */
  public static function SessionDelete()
  {
      // Loop all arguments
    foreach (func_get_args() as $argument) {
        // Array element
      if (is_array($argument)) {
          // Loop the keys
        foreach ($argument as $key) {
            // Unset session key
          unset($_SESSION[(string) $key]);
        }
      } else {
          // Remove from array
        unset($_SESSION[(string) $argument]);
      }
    }
  }

  /**
   *  Destroy session
   *
   *  <code>
   *    App::SessionDestroy();
   *  </code>
   *
   *  @access  public
   */
  public static function SessionDestroy()
  {
      // Destroy
    if (session_id()) {
        session_unset();
        session_destroy();
        $_SESSION = array();
    }
  }
  /**
   *  Check session
   *
   *  <code>
   *    App::SessionExists();
   *  </code>
   *
   *  @access  public
   */
  public static function SessionExists()
  {
      // Start session if needed
    if (!session_id()) {
        self::SessionStart();
    }
    // Loop all arguments
    foreach (func_get_args() as $argument) {
        // Array element
      if (is_array($argument)) {
          // Loop the keys
        foreach ($argument as $key) {
            // Does NOT exist
          if (!isset($_SESSION[(string) $key])) {
              return false;
          }
        }
      } else {
          // Does NOT exist
        if (!isset($_SESSION[(string) $argument])) {
            return false;
        }
      }
    }

      return true;
  }

  /**
   *  Get session
   *
   *  <code>
   *    App::SessionGet('user_id');
   *  </code>
   *
   *  @access  public
   */
  public static function SessionGet($key)
  {
    // Start session if needed
    if (!session_id()) {
        self::SessionStart();
    }
    // Redefine key
    $key = (string) $key;
    // Fetch key
    if (self::SessionExists((string) $key)) {
        return $_SESSION[(string) $key];
    }
    // Key doesn't exist
    return;
  }
  /**
   *  Set session
   *
   *  <code>
   *    App::SessionSet('user_id',$id);
   *  </code>
   *
   *  @access  public
   *
   *  @param  string $key   key
   *  @param  string $value   value
   */
  public static function SessionSet($key, $value)
  {
      // Start session if needed
    if (!session_id()) {
        self::SessionStart();
    }
    // Set key
    $_SESSION[(string) $key] = $value;
  }

  /**
   *  Add actions in theme
   *
   *  <code>
   *    App::addAction('foo',callback,10,array());
   *  </code>
   *
   *  @access  public
   *
   *  @param  string $name  name
   *  @param  function $funt  function
   *  @param  number $priority  priority
   *  @param  array $args   null
   */
  public static function AddAction($name, $func, $priority = 10, array $args = null)
  {
      // Hooks a function on to a specific action.
    static::$actions[] = array(
      'name' => (string) $name,
      'func' => $func,
      'priority' => (int) $priority,
      'args' => $args,
    );
  }

  /**
   *  Run actions
   *
   *  <code>
   *    App::RunAction('user_id',array(),false);
   *  </code>
   *
   *  @access  public
   *
   *  @param  string $name  name
   *  @param  array $args   array
   *  @param  bolean $return  false
   */
  public static function RunAction($name, $args = array(), $return = false)
  {
      // Redefine arguments
    $name = (string) $name;
      $return = (bool) $return;
    // Run action
    if (count(static::$actions) > 0) {
        // Sort actions by priority
      $actions = self::ArrShort(static::$actions, 'priority');
      // Loop through $actions array
      foreach ($actions as $action) {
          // Execute specific action
        if ($action['name'] == $name) {
            // isset arguments ?
          if (isset($args)) {
              // Return or Render specific action results ?
            if ($return) {
                return call_user_func_array($action['func'], $args);
            } else {
                call_user_func_array($action['func'], $args);
            }
          } else {
              if ($return) {
                  return call_user_func_array($action['func'], $action['args']);
              } else {
                  call_user_func_array($action['func'], $action['args']);
              }
          }
        }
      }
    }
  }

  /**
   * Zip files
   * @param string $source directory to zip
   * @param string $dest   output
   */
  public function Zip($name,$source,$dest)
  {
    // Get real path for our folder
    $rootPath = realpath($source);
    // Initialize archive object
    $zip = new ZipArchive();
    $zip->open($name.'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
    // Create recursive directory iterator
    /** @var SplFileInfo[] $files */
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator($rootPath),
        RecursiveIteratorIterator::LEAVES_ONLY
    );

    foreach ($files as $name => $file)
    {
        // Skip directories (they would be added automatically)
        if (!$file->isDir())
        {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);

            // Add current file to archive
            $zip->addFile($filePath, $relativePath);
        }
    }
    // Zip archive will be created only after closing object
    $zip->close();
  }


  /**
   *  launch routes
   *  @access  public
   */
  public function launch()
  {
      // Require libraries
    include LIBRERIA.'/medoo.php';
    // Turn on output buffering
    ob_start();
    // Sanitize url
    self::runSanitizeURL();
    // load config
    $this->LoadConfig();
    // load lenguage
    $this->LoadLanguage();
    // zona horaria por defecto
    date_default_timezone_set(self::$config['timezone']);
    // Send default header and set internal encoding
    header('Content-Type: text/html; charset=UTF-8');
      function_exists('mb_language') and mb_language('uni');
      function_exists('mb_regex_encoding') and mb_regex_encoding('UTF-8');
      function_exists('mb_internal_encoding') and mb_internal_encoding('UTF-8');
    // Gets the current configuration setting of magic_quotes_gpc and kill magic quotes
    if (get_magic_quotes_gpc()) {
        function stripslashesGPC(&$value)
        {
            $value = stripslashes($value);
        }
        array_walk_recursive($_GET, 'stripslashesGPC');
        array_walk_recursive($_POST, 'stripslashesGPC');
        array_walk_recursive($_COOKIE, 'stripslashesGPC');
        array_walk_recursive($_REQUEST, 'stripslashesGPC');
    }
    // Start the session
    self::SessionStart();
    // launch
    $url = $_SERVER['REQUEST_URI'];
      $base = str_replace('\\', '/', dirname($_SERVER['SCRIPT_NAME']));
      if (strpos($url, $base) === 0) {
          $url = substr($url, strlen($base));
      }
      $url = trim($url, '/');
      foreach ($this->routes as $pattern => $callback) {
          if (preg_match($pattern, $url, $params)) {
              array_shift($params);

              return call_user_func_array($callback, array_values($params));
          }
      }
    // Flush (send) the output buffer and turn off output buffering
    ob_end_flush();
  }
}
