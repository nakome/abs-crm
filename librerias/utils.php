<?php

defined('ACCESSO') or die('No direct script access.');


/**
 * @author Lorena Gomez Gonzalez  <mayantigo@gmail.com>
 * @version  1.0.0
 * @todo  extra public functions to extend router.php
 * @uses  Medoo to work with sqlite
 */


/**
*  @package: Assets();
*  @todo function para acortar enlace estilos, javascript
*  @example  Assets();
*/
if (!function_exists('Assets')) {
  function Assets()
  {
    $url = App::Url().'/plantilla/assets';
    return $url;
  }
}

/**
*  @package: logo();
*  @todo crear espacios en blanco en html
*/
if (!function_exists('logo')) {
  function logo()
  {
    if (App::FileExists(ROOT.'/plantilla/assets/img/logo.png')) {
      return '<img src="'.App::Url().'/plantilla/assets/img/logo.png">';
    } else {
      return App::$lang['name'];
    }
  }
}

/**
*  @package: nbsp();
*  @todo crear espacios en blanco en html
*/
if (!function_exists('nbsp')) {
  function nbsp($num = 1)
  {
    $result = '';
    for ($i = 0; $i < $num; ++$i) {
      $result .= '&nbsp;';
    }

    return $result;
  }
}

/**
*  @package: isLogin();
*  @todo Comprueba si esta logueado
*/
if (!function_exists('isLogin')) {
  function isLogin()
  {
    if (!App::SessionGet('app_uid') && !App::SessionGet('app_email')) {
      App::Redirect(App::Url().'/entrar');
    }
  }
}

/**
*  @package: isActive();
*  @todo añadir classe en el link activo
*/
if (!function_exists('isActive')) {
  function isActive($link)
  {
    $num = (int) count(App::GetUriSegments()) - 1;
    if (App::GetUri($num) == $link) {
      return 'class="active"';
    }
  }
}

/**
*  @package: setDb();
*  @todo funcion para acortar la inserccion
*/
if (!function_exists('setDb')) {
  function setDb($database, $data, $redirect)
  {
    $db = App::Db()->insert($database, $data);
    if ($db) {
      App::SetMsg(App::$lang['success'], App::$lang['the_file_has_been_added']);
      App::Redirect(App::Url().$redirect);
    }
  }
}

/**
*  @package: putDb();
*  @todo funcion para acortar la actualizacion
*/
if (!function_exists('putDb')) {
  function putDb($database, $data, $item_id, $id, $redirect)
  {
    $db = App::Db()->update($database, $data, array($item_id => $id));
    if ($db) {
      App::SetMsg(App::$lang['success'], App::$lang['the_file_has_been_update']);
      App::Redirect(App::Url().$redirect);
    }
  }
}

/**
*  @package: delDb();
*  @todo funcion para borrar id
*/
if (!function_exists('delDb')) {
  function delDb($database, $item_id, $id, $redirect)
  {
    // borramos
    App::Db()->delete($database, array('AND' => array($item_id => $id)));
    // enviamos mensaje
    App::SetMsg(App::$lang['success'], App::$lang['the_file_has_been_delete']);
    // refreshcamos
    App::Redirect(App::Url().$redirect);
  }
}

/**
*  @package: onlyAdmin;
*  @todo funcion que solo da accesso al administrador
*/
if (!function_exists('onlyAdmin')) {
  function onlyAdmin()
  {
    if (App::SessionGet('app_role') != 'administrador') {
      App::Redirect(App::Url());
    }
  }
}

