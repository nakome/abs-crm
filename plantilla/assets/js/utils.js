(function() {

  /**
   * codificar base64
   * $('#searchinput').encode_base64('tableid');
   */
  $.fn.encode_base64 = function(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
  };

  /**
   * decodificar base64
   * $('#searchinput').decode_base64('tableid');
   */
  $.fn.decode_base64 = function(str) {
    return decodeURIComponent(escape(window.atob(str)));
  };

  /**
   * Quitar acentos
   * $('#searchinput').removeAcents($('#searchinput').val());
   */
  $.fn.removeAccents = function(str) {
    var accents    = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    var strLen = str.length;
    var i, x;
    for (i = 0; i < strLen; i++) {
      if ((x = accents.indexOf(str[i])) != -1) {
        str[i] = accentsOut[x];
      }
    }
    return str.join('');
  };




  /**
   * Buscar en la table
   * $('#searchinput').filterTable('tableid');
   */
  $.fn.filterTable = function() {
    this.keyup(function() {
      // convert first value to uppercase on keyup
      this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1);
      var data = this.value.split(" ");
      //create array
      var tH = '';
      if ($('#lista').length) tH = $("#lista").find('li');
      if ($('#tbody').length) tH = $("#tbody").find("tr");
      if (this.value === "") {
        tH.show();
        return;
      }
      //hide all the rows
      tH.hide();
      //Recusively filter the jquery object to get results.
      tH.filter(function(i, v) {
        var $t = $(this);
        for (var d = 0; d < data.length; ++d) {
          var f = data[d];
          if ($t.is(":contains('" + f + "')")) {
            return true;
          }
        }
        return false;
      }).show();
    });
  };
})(jQuery);






// Original code: http://stackoverflow.com/a/14268260/1163000
// Usage: `makeSortable(elem);`
(function() {
  function sortTable(table, col, reverse) {
    var tb = table.tBodies[0], // Use `<tbody>` to ignore `<thead>` and `<tfoot>` rows
      tr = Array.prototype.slice.call(tb.rows, 0); // Put rows into array
    reverse = -((+reverse) || -1);
    tr = tr.sort(function(a, b) { // Sort rows
      return reverse * (a.cells[col].textContent.trim().localeCompare(b.cells[col].textContent.trim()));
    });
    for (var i = 0, len = tr.length; i < len; ++i) {
      tb.appendChild(tr[i]); // Append each row in order
    }
  }

  function makeSortable(table) {
    var th = table.tHead,
      i;
    th && (th = th.rows[0]) && (th = th.cells);
    if (th) {
      i = th.length;
    } else {
      return; // If no `<thead>` then do nothing
    }
    while (--i >= 0)(function(i) {
      var dir = 1;
      th[i].onmousedown = function() {
        for (var j = 0, jen = th.length; j < jen; ++j) {
          th[j].className = th[j].className.replace(/(^| )desc|asc( |$)/g, "$1$2");
        }
        sortTable(table, i, (dir = 1 - dir));
        this.className += dir === 1 ? " desc" : " asc";
        return false;
      };
    }(i));
  }
  window.makeSortable = makeSortable;
})();

