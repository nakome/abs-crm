$(document).ready(function() {
  $.material.init(); // cargando material
  $("#searchthis").filterTable(); // filtro busqueda para la tabla
  if (document.querySelector('#table')) makeSortable(document.querySelector('#table')); // filtro para la tabla
  $('.preloader').fadeOut('slow'); // ocultando preloader
  // Javascript para hacer el id de los tabs activo
  var url = document.location.toString();
  if (url.match('#')) {
    $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
  }
  // Cambia el hash al refreshcar
  $('.nav-tabs a').on('shown.bs.tab', function(e) {
    window.location.hash = e.target.hash;
  });
  // tooltips
  $('[data-toggle="tooltip"]').tooltip();

  $('input[type="date"]').bootstrapMaterialDatePicker({time: false});

  $('#search').on('submit', function(e) {
      e.preventDefault();
      return false;
  });
  $('#searchbyOrder').on('submit', function(e) {
      e.preventDefault();
      return false;
  });
});

