<?php defined('ACCESSO') or die('No direct script access.');

// array de valores
return array(
  'nombre_empresa' => 'Sat informatica',
  'lenguaje' => 'es', // lenguaje usado
  'base_de_datos' => 'sqlite', // base de datos
  'mysql' => array( // configuarion base de mysql
    'database_type' => 'mysql', // tipo de base de datos
    'database_name' => 'taller', // nombre de la base de datos
    'server' => 'localhost', // servidor base de datos
    'username' => 'moncho', // usuario base de datos
    'password' => 'demo', // password base de datos
    'charset' => 'utf8' // charset base de datos
  ),
  'sqlite' => array( // configuracion sqlite
    'database_type' => 'sqlite', // tipo de la base de datos
    'database_file' => DATABASE.'/db.sqlite' // ruta de la base de datos
  ),
  'pagination' => 20,
  'timezone' => 'Europe/Madrid'
);
