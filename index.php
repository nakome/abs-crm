<?php if (version_compare(PHP_VERSION, '5.5.0', '<'))  exit('Panel requires PHP 5.5.0 or greater.');

// carpeta root
define('ROOT', rtrim(dirname(__FILE__), '\\/'));
// definir accesso para los archivos php
define('ACCESSO', true);

// definir rutas de carpetas
define('CONFIGURACION', ROOT.'/configuracion');
define('DATABASE', ROOT.'/database');
define('LIBRERIA', ROOT.'/librerias');
define('RUTAS', ROOT.'/rutas');
define('LENGUAJES', ROOT.'/lenguajes');
define('IMAGENES', ROOT.'/imagenes');
define('PLANTILLA', ROOT.'/plantilla');
define('BACKUP', ROOT.'/backup');

// definir archivos de partes html (head,footer etc..)
define('INCLUDES', ROOT.'/plantilla/includes');

// modo desarrollo para ver los errores
define('DESARROLLO', true);
if (DESARROLLO) {
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  ini_set('track_errors', 1);
  ini_set('html_errors', 1);
  error_reporting(E_ALL | E_STRICT | E_NOTICE);
}else{
  ini_set('display_errors', 0);
  ini_set('display_startup_errors', 0);
  ini_set('track_errors', 0);
  ini_set('html_errors', 0);
  error_reporting(0);
}

// Libreria para las rutas
require_once LIBRERIA.'/app.php';
// Utilidades
require_once LIBRERIA.'/utils.php';
// rutas
require_once RUTAS.'/rutas.php';

