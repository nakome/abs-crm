# Abs-crm

### Estructura

       .htaccess
    │   index.php
    │   README.md
    │
    ├───backup
    │       db.sqlite
    │       imagenes.zip
    │
    ├───configuracion
    │       config.php
    │
    ├───database
    │       db.sqlite
    │
    ├───imagenes
    │   ├───empresa
    │   └───productos
    │
    ├───lenguajes
    │       en.php
    │       es.php
    │
    ├───librerias
    │       app.php
    │       medoo.php
    │       utils.php
    │
    ├───plantilla
    │   │   gestion.html
    │   │   index.html
    │   │   preview_lista_compra.html
    │   │   preview_os_servicio.html
    │   │
    │   ├───assets
    │   │   ├───css
    │   │   │       app.css
    │   │   │       preview.css
    │   │   │
    │   │   ├───img
    │   │   │       bg-login.jpg
    │   │   │       loader.svg
    │   │   │       logo-factura.png
    │   │   │       logo.png
    │   │   │       logo.psd
    │   │   │       menu-active.png
    │   │   │
    │   │   ├───js
    │   │   │       app.js
    │   │   │       utils.js
    │   │   │
    │   │   └───vendor
    │   │       ├───autocomplete
    │   │       ├───bootstrap
    │   │       │   ├───css
    │   │       │   │       bootstrap-material-design.min.css
    │   │       │   │       bootstrap.min.css
    │   │       │   │       ripples.min.css
    │   │       │   │
    │   │       │   ├───fonts
    │   │       │   │
    │   │       │   └───js
    │   │       │           bootstrap.min.js
    │   │       │           material.min.js
    │   │       │           ripples.min.js
    │   │       │
    │   │       ├───chart
    │   │       ├───iconfont
    │   │       ├───jquery
    │   │       ├───trumbowyg
    │   │       └───datapicker
    │   │
    │   ├───includes
    │   │       footer.html
    │   │       head.html
    │   │       navbar.html
    │   │       notificacion_productos.html
    │   │       sidebar.html
    │   │
    │   └───rutas_html
    │       │   categorias.html
    │       │   categorias_producto.html
    │       │   clientes.html
    │       │   dashboard.html
    │       │   empresa.html
    │       │   listas_compra.html
    │       │   marcas.html
    │       │   os_servicio.html
    │       │   preview_os_servicio.html
    │       │   productos.html
    │       │   proveedores.html
    │       │   servicios.html
    │       │   usuarios.html
    │       │
    │       ├───editar
    │       │       editar_cliente.html
    │       │       editar_lista_compra.html
    │       │       editar_os_servicio.html
    │       │       editar_producto.html
    │       │       editar_servicio.html
    │       │       editar_usuario.html
    │       │
    │       └───nueva
    │               nueva_lista_compra.html
    │               nueva_os_servicio.html
    │               nuevo_cliente.html
    │               nuevo_producto.html
    │               nuevo_servicio.html
    │               nuevo_usuario.html
    │
    └───rutas
            rutas.php
            ruta_categorias.php
            ruta_clientes.php
            ruta_empresa.php
            ruta_listacompra.php
            ruta_marcas.php
            ruta_notas.php
            ruta_ordenes_servicio.php
            ruta_productos.php
            ruta_proveedores.php
            ruta_servicios.php
            ruta_usuarios.php


# Configuración

    'nombre_empresa' => 'Sat informatica',
    'lenguaje' => 'es', // lenguaje usado en o es
    'base_de_datos' => 'sqlite', // base de datos
    'sqlite' => array( // configuracion sqlite
      'database_type' => 'sqlite', // tipo de la base de datos
      'database_file' => DATABASE.'/db.sqlite' // ruta de la base de datos
    ),
    'pagination' => 5, // paginacion
    'timezone' => 'Europe/Madrid' // zona horaria




**Password para el usuario Root:**
- usuario root@root.com
- contraseña root




**Version de la aplicación:** 1.0.0



## App Class Framework ( Documentación en Ingles )

### Functions to make routes

   
    <?php
      $app =  new App();
      // go to http://localhost/foo
      $app->Route('/foo',function(){
       echo 'I am in foo';
      });
      // go to http://localhost/foo/1
      $app->Route('/foo/(:num)',function($num){
       echo 'I am in foo '.$num;
      });
      // go to http://localhost/foo/bar
      $app->Route('/foo/(:any)',function($any){
       echo 'I am in foo '.$any;
      });
   

---

## Methods

### getMsg()
_Get Notifications_

    <?php App::getMsg();

### setMsg()
_Set Notifications._

    <?php App::setMsg('Success','data saved!');

### Db() 
_Init database_

    <?php $this->Db();

### ValidEmail()
_Check valid mail_

    <?php App::ValidEmail('jshon@gmail.com')
 
### Post()

_Post data_

    <?php App::Post($key); = $_POST['KEY'];
  
### Get()

_Get data_

    <?php App::Get($key); = $_Get['KEY'];

### cleanString()
_Clear tags in file_

    <?php App::cleanString(); 

### Json()
_Convert array to json_

    <?php
        $array('title' => 'foo');
        App::Json($array);

### ScanDir() 
_Scan directories_

    <?php App::ScanDir('blog');  

### FileExt()
_Get extension of file_

    <?php App::FileExt('file.txt');

### FileName()
_Get filename_

    <?php App::FileName('file.txt');

### FileScan()
_Scan files_

    <?php App::FileScan('file.txt',array('txt','md')); 

### FileExists() 
_Check file if exists_

    <?php App::FileExists('file.txt');

### FileGetContent()
_Get files_

    <?php App::FileGetContent($file);

### Url()
_Get base url_

    <?php App::Url();

### CurrentUrl()
_Get current url_

    <?php App::CurrentUrl();

### getUriSegments()
_Get uri segments in array_

    <?php App::getUriSegments()

### getUri()
_Get uri segment_

    <?php App::getUri('blog')

### Redirect()
_Redirect to another place_

    <?php App::Redirect('foo.html',302,1);  

### ParseUrl()
_Sanitize Pretty url for SEO_

    <?php App::ParseUrl('this is a new world'); = this-is-a-new-worl  

### sanitizeURL() 
_Sanitize Url_

    <?php App::sanitizeURL('url');
 
### runSanitizeURL()

    <?php App::runSanitizeURL() 

### GenerateToken()
_Generate token_

    <?php App::GenerateToken();
 
### CheckToken()
_Check token_

    <?php App::CheckToken($t);
 
### ArrSet()
_Set array_

    <?php App::ArrSet($array,'foo.title','value');
    
### ArrShort()
_Short array values_

    <?php App::ArrShort($array,'date','DESC'); 

### ArrGet() 
_Get array data_

    <?php App::ArrGet($array,'foo.title');  

### Route()
_Render Assets_

    <?php
        $App->Route('/foo/(:any)',function($any){
          echo $any;
        }); 


### SessionStart() 
_Start session_

    <?php App::SessionStart();

### SessionDelete()
_Remove session_

    <?php App::SessionDelete();

### SessionDestroy()
_Destroy session_

    <?php App::SessionDestroy();

### SessionExists()
_Check session_

    <?php App::SessionExists();

### SessionGet()
_Get session_

    <?php App::SessionGet('user_id');

### SessionSet()
_Set session_

    <?php App::SessionSet('user_id',$id);

### launch()
_launch routes_

    <?php App::launch();

### LoadConfig() 
_Get config_

    <?php $this->LoadConfig();

### LoadLanguage()
_Get language_

    <?php $this->LoadLanguage();